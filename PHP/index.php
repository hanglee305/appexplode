<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
        integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link  rel="stylesheet" href="css/style.css">
    <link rel="icon" href="img/logo.svg">
    <link href="https://fonts.googleapis.com/css?family=Lato|Roboto&display=swap" rel="stylesheet">
    <title>AppExlode</title>
</head>

<body>
    <div class="head ">
        <nav class="navbar navbar-expand-lg navbar-light bg-light ">

            <a class="navbar-brand" href="#">
                <img src="img/logo.svg" width="50" alt="logo"><span class="text-dark"> AppExlode</span>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="  collapse navbar-collapse justify-content-end" id="navbarNav">
                <ul class="navbar-nav ">
                    <li class="nav-item active mx-2">
                        <a class="nav-link text-dark" href="#">Home </a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="#">About us</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="#">Portfollo</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="#">Approach</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="#">Team</a>
                    </li>
                    <li class="nav-item mx-2">
                        <a class="nav-link text-dark" href="#">Contact Us</a>
                    </li>
                    <button class="btn btn-outline-primary btn-nav text-white ml-2 submit"
                        style="background-color:#8e44ad">
                        Instant Quote
                    </button>
                </ul>
            </div>
        </nav>

        <div class="banner container-fluid" style="padding: 4rem;;  background: url(img/banner.jpg) no-repeat center center/cover;">
            <div class="row">
                <div class="col-lg-6 col">
                    <h1 class="text-uppercase text-center justify-content-center mx-5 text-white font-weight-bold process-title-header"
                        >build your dream</h1>
                </div>
                <div class="col-lg-6 m-auto col" >
                    <p class="text-white text-center justify-content-center header-content" >The
                        App
                        Explode
                        ethos is to bring digital
                        dreams to reality. If you
                        have a vision for an app or a digital product (apps, websites, games), we are the team to build
                        it. </p>
                </div>
            </div>
        </div>

    </div>
    <!-- who are you -->
    <section class="container-fluid who-body" style="padding-left:3rem ;">
        <div class="row mt-3 ">
            <div class="col-lg-5">
                <h1 class="font-weight-bold mb-4 process-title" >Who we are</h1>
            </div>
            <div class="col-lg-7 ">
                <p class=" mt-3">App Explode specialises in helping ideas come to life quick. We help you finese
                    your
                    idea, work
                    through designs, and produce a rapid prototype in as little as 3 weeks.</p>
            </div>
        </div>

        <div class="row  ">
            <div class="col-lg-4 my-4">
                <p><b class="text-capitalize">Turn your app idea into reality, FAST</b>.<br>Build it and
                    they will come is a myth. We work with
                    entrepreneurs and innocators to bring their app ideas to life and help them find their first
                    users
                    with proven early adopter customer acquisition strategies.</p>
                <button class="btn btn-outline-info text-light text-uppercase submit "
                    style="background: #8e44ad;">instant
                    quote</button>
            </div>
            <div class="col-lg-8 mt-4 ">
                <img src="img/phone.jpg" class=" img w-100" width="430" height="700" alt="phone">
            </div>
        </div>

    </section>
    <!-- our process -->
    <section class="container-fluid  process" style="padding: 1rem;">
        <h1 class="text-white font-weight-bold m-5 process-title process-title1" >Our process</h1>
        <div class="  justify-content-center">

            <div class="row mb-4  content">

                <div class="col-lg-6 col-sm-6  m-auto ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">1.</h2>
                        <h2 class="text-white text-capitalize">Chat with an app expert</h2>
                        <p class="text-white d-block">Speak with one of our friendly app experts. We're happy to sign an
                            NDA! We
                            work together using our app quote form to define key features of your app and bring the idea
                            to life. Book in a
                            time here.</p>

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6 m-auto ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">2.</h2>
                        <h2 class="text-white text-capitalize">your app quote in 48 hours</h2>
                        <p class="text-white d-block">With the information collected in the first step, the App Explode
                            team
                            will plan out all the design and development requirements for your app, and deliver your
                            quote
                            fast. Prices range from $10,000 - $150,000.</p>
                    </div>
                </div>
            </div>


            <div class="row mb-4  content">

                <div class="col-lg-6 col-sm-6   m-auto ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">4.</h2>
                        <h2 class="text-white text-capitalize">start with design</h2>
                        <p class="text-white">Once the project is agreed, design begins! Planning and design starts
                            prior to
                            development, and
                            AppExplode apps are always a thing of beauty.We also do a kickoff meeting to</p>

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6  m-auto  ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">3.</h2>
                        <h2 class="text-white text-capitalize">coding begins</h2>
                        <p class="text-white">Development of your app begins after just a week of design work, and
                            continues
                            on concurrenetly white
                            the rest of the screen designs are finished. We work with React</p>
                    </div>
                </div>
            </div>
            <div class="row mb-4  content">

                <div class="col-lg-6 col-sm-6   m-auto ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">5.</h2>
                        <h2 class="text-white text-capitalize">app launch in 12 weeks.</h2>
                        <p class="text-white mb-4">We work hanrd to get your app to market within 12 Weeks. This
                            requires
                            focus,
                            planning and concurrent
                            design and development. Some apps might require more time, but speed to market is our goal.
                        </p>

                    </div>
                </div>
                <div class="col-lg-6 col-sm-6   m-auto ">
                    <div class="process-content">
                        <h2 class="text-white text-capitalize text-center">6.</h2>
                        <h2 class="text-white text-capitalize">support & marketing</h2>
                        <p class="text-white mb-4">Once your app is built, the next phase of your journey strategy and
                            partnerships, and support you any further development requirements. Includes 60 day bug
                            fixing.
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- client -->
    <section class="container-fluid bg-light mb-2" style="padding: 2rem;">
        <h1 class="text-dark font-weight-bold m-5 process-title process-title-clinet " >What our client's say
        </h1>

        <div id="mycarousel" class="carousel slide carousel-dong" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item ">
                    <img src="img/2.png" class="d-block img-what justify-content-center m-auto " height="220"
                        width="160" alt="">
                </div>

                <div class="carousel-item active">
                    <img src="img/people.jpg" class="d-block img-what justify-content-center mx-auto " width="400"
                        height="220" alt="people">
                </div>


            </div>
            <a class="carousel-control-prev" href="#mycarousel" role="button" data-slide="prev">
                <span aria-hidden="true"><img src="img/back.svg" alt="back"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#mycarousel" role="button" data-slide="next">
                <span aria-hidden="true"><img src="img/next.svg" alt=""></span>
                <span class="sr-only">Next</span>
            </a>
            <div class=" d-none d-md-block text-center ">
                <p class="d-block clinet-content"><span class="text-warning font-weight-bold text-center d-block"
                        style="font-size: 100px;">&ldquo;</span>As a valued partner that understands the needs
                    of startups, I cant recommnent app.</p>

                <div class="mb-5">
                    <h5 class="text-dark text-uppercase">patrick anderrson</h5>
                    <p class="mb-5">Founder - Dressum</p>
                </div>
            </div>

        </div>

    </section>
<!-- what we do -->
    <section class="container-fluid  bg-light what" style="padding: 1rem;">
        <div class="row  mx-4">
            <div class="col-lg-6 col-sm-6">
                <h1 class="text-dark font-weight-bold mb-3 process-title text-capitalize " >what
                    we do</h1>
            </div>
            <div class="col-lg-6 mt-4">
                <p class="what-content">App Explore's experence can be the difference between struggling and accelerated growth. If
                    you want
                    your user experience to be top-notch and your business impact at its best, we're the people who can
                    help you get there.</p>
            </div>
        </div>
        <div class="row mt-4 card-the" >
            <div class="col-lg-6 col-sm-6 mb-3">
                <div class="px-3">
                    <div class=" mb-3 card-shadow card-radius " style="max-width: 540px;">
                        <div class="row">
                            <div class="col-md-4">
                                <img src="img/icon1.jpg" class="card-img m-3" alt="icon1">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body justify-content-center">
                                    <h5 class="card-title">App</h5>
                                    <p class="card-text">Get hands-on guidance in positioning your product in the
                                        market.<br>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6 col-sm-6 mb-3 ">
                <div class="px-3">
                    <div class=" mb-3 card-shadow card-radius" style="max-width: 540px;">
                        <div class="row no-gutters ">
                            <div class="col-md-4">
                                <img src="img/icon2.jpg" class="card-img m-3" alt="icon1">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body justify-content-center">
                                    <h5 class="card-title">Websites</h5>
                                    <p class="card-text">Discover the power of research in improving your business in
                                        leaps
                                        and
                                        bounds.

                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 mb-3">
                <div class="px-3">
                    <div class="card-lg mb-3 card-shadow card-radius" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="img/icon3.jpg" class="card-img m-3" alt="icon1">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">Games</h5>
                                    <p class="card-text">Find new angles to improve your product with well-planned,
                                        cost-effective
                                        user
                                        experience
                                        changes.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-sm-6 mb-3 ">
                <div class="px-3">
                    <div class="card-lg mb-3 card-shadow card-radius" style="max-width: 540px;">
                        <div class="row no-gutters">
                            <div class="col-md-4">
                                <img src="img/icon4.jpg" class="card-img m-3" alt="icon1">
                            </div>
                            <div class="col-md-8">
                                <div class="card-body">
                                    <h5 class="card-title">UX/UI</h5>
                                    <p class="card-text ">Stop focusing on putting out fires and map out your growth to
                                        bring your product at the next level.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="container-fluid bg-white my-5 get-body" style="padding: 2rem;" id="form">
    <div class="row my-3 ">
            <div class="col-lg-8 mx-4 get-header-1">
                <div class="get-header">
                    <h1 class="font-weight-bold mr-5 process-title get-header" >Get in touch with the team behind the
                        apps</h1>
                    <p class="mr-4 mt-4">App Explode is a Sydney based cohort of designers, developers and technologists
                        that
                        love breathing
                        life into great ideas. Call us today on 0420 922215 or contact us via the form below.</p>

                </div>
            </div>
            <div class="col-lg-3 ml-0 float-right-lg">
                <ul class="list-unstyled mx-4 mt-3 get-content  get-header">
                    <li class="text-decoration-none text-uppercase h6">Phone</li>
                    <li class="text-decoration-none text-uppercase h4">+85 234 385 7677
                    </li>
                    <li class="text-decoration-none text-uppercase h6">email</li>
                    <li class="text-decoration-none t h4">team@appexplode.com</li>

                </ul>
            </div>
        </div>
<div class="row mb-3 ">
            <div class="col-lg-4 col-sm-6 ml-3 my-3">

                <form action="form.php" method="POST" class="contact-group mt-4" >
        
               <div class="form-group">
                   <input type="text" class="form-control mb-2 mr-sm-2 input" required="" name="name" placeholder="Your Name*">
                   <label for="name" class="label1">Your Name*</label>
               </div>
               <div class="form-group">
                   <input type="email" class="form-control mb-2 mr-sm-2 input" required="" name="email" placeholder="Your Email*">
                   <label for="email" class="label1">Your Email*</label>
               </div>
               <div class="form-group">
                   <input type="phone" class="form-control mb-2 mr-sm-2 input" required="" name="phone" placeholder="Your Phone Number*">
                   <label for="phone" class="label1">Your Phone Number*</label>

               </div>
               <div class="form-group">
                   <input type="website" class="form-control mb-2 mr-sm-2 input" required=""name="website" placeholder="Your Website*">
                   <label for="website" class="label1" >Your Website*</label>
               </div>
               <div class="form-group">
                   <textarea name="comment" id="message" class="input w-100  "></textarea>
                   <label for="comment" class="label1 textarea ">Tell us briefly about your app idea</label>
               </div>
               <div>
                   <button type="submit" class="btn btn-outline-info text-uppercase submit text-light mt-3"
                       style="background-color: #9b59b6;">submit</button>
               </div>

           </form>
           
           </div>
            <div class="col-lg-7">
                <div>
                    <img src="img/mes.png" alt="mes" class="w-100">
                </div>
            </div>
        </div>

        </section>
            
    <footer class="container-fluid my-4 mt-5 footer1" style="padding-left: 4rem;">
        <div class="row">
            <div class="col-lg-3 col-sm-6">
                <a class="navbar-brand" href="#">
                    <img src="img/logo.svg" width="50" alt="logo"><span class="text-dark h5 font-weight-bold">
                        AppExlode</span>
                </a>
                <ul class="list-unstyled">
                    <li class="text-decoration text-dark my-3  font-weight-bold h5 text-capitalize">business hours</li>
                    <li class="text-decoration text-dark my-3">Monday - Friday 08:00 AM to 06:00 PM</li>
                    <li class="text-decoration text-dark my-3 ">Saturday - Closed</li>
                    <li class="text-decoration text-dark my-3 ">Sunday - Closed</li>
                    <li class="rowmx-2 ">
                        <img src="img/logo-facebook.png" class="mx-2" alt="">
                        <img src="img/logo-instagram.png" class="mx-2" alt="">
                        <img src="img/Path.png" class="mx-2" alt="">
                    </li>
                    <li class="text-decoration text-dark my-3">Appexplode, 2019, All rights resverved</li>

                </ul>
            </div>
            <div class="col-lg-3 col-sm-6 ">
                <h5 class="font-weight-bold">Contact</h5>
                <ul class="list-unstyled">
                    <li class="text-decoration-none my-3"><img src="img/letter.png" class="" alt="">
                        team@appexplode.com
                    </li>
                    <li class="text-decoration-none my-3"><img src="img/ic_phone.png" class="" alt=""> LLOYD +61 40 444
                        1243</li>
                    <li class="text-decoration-none my-3"><img src="img/ic_phone.png" class="" alt=""> PAM +61 420 922
                        215</li>
                </ul>
            </div>
            <div class="col-lg-3 col-sm-6">
                <h5 class="font-weight-bold">Company</h5>
                <ul class="list-unstyled">
                    <li class="text-decoration-none my-3"> Who We are
                    </li>
                    <li class="text-decoration-none my-3"> How it work
                    </li>
                    <li class="text-decoration-none my-3">Team</li>
                    <li class="text-decoration-none my-3">Instant Quote</li>
                </ul>
            </div>
            <div class="col-lg-3 col-sm-6">
                <h5 class="font-weight-bold">Help</h5>
                <ul class="list-unstyled">
                    <li class="text-decoration-none my-3"> Terms and Conditions
                    </li>
                    <li class="text-decoration-none my-3">Privacy Policy
                    </li>
                </ul>
            </div>
        </div>
        <div>
            <p class="lead small"><i>*We don't share your personal info or idea with anyone. We are happy to sign on MDA
                    before full of an idea. Check out our Privacy Policy for more information.</i></p>
        </div>
    </footer>

    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>



</body>

</html>
